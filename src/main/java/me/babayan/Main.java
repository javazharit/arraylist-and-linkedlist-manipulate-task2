package me.babayan;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    private static final int NUMBER_OF_ELEMENTS = 100000;

    public static void main(String[] args) {
		MyClass[] array = new MyClass[NUMBER_OF_ELEMENTS];

		System.out.println(String.format("Creating %d instances of \"MyClass\" type.", NUMBER_OF_ELEMENTS));
		for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
			array[i] = new MyClass(String.valueOf(i));
		}
		System.out.println("Done.\n");

        List<MyClass> arrayList = new ArrayList<>();
        List<MyClass> linkedList = new LinkedList<>();

        System.out.println("=-=-=-=-=-=-=Add-=-=-=-=-=-=-=-=-=-=-=-=");
        /**
         * adding time to collection
         * */
        long srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            arrayList.add(array[i]);
        }
        long finishTime = System.currentTimeMillis();
        long res = finishTime - srartTime;
        System.out.println("arrayList: " + res + " milliseconds");

        srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            linkedList.add(array[i]);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("listLinked: " + res + " milliseconds\n");


        System.out.println("=-=-=-=-=-=-=Read-=-=-=-=-=-=-=-=-=-=-=-=");
        /**
         * collection element reading time
         *
         * */
        srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            arrayList.get(i);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("arrayList: " + res + " milliseconds");

        srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            linkedList.get(i);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("listLinked: " + res + " milliseconds\n");


        System.out.println("=-=-=-=-=-=-=Update/Insert-=-=-=-=-=-=-=-=-=-=-=-=");
        /**
         * update collection element
         * */
        srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            arrayList.set(i, array[i]);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("arrayList: " + res + " milliseconds");

        srartTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            linkedList.set(i, array[i]);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("listLinked: " + res + " milliseconds\n");


        System.out.println("=-=-=-=-=-=-=Delete-=-=-=-=-=-=-=-=-=-=-=-=");
        /**
         * collection element deleting time
         * */
        srartTime = System.currentTimeMillis();
        for (int i = NUMBER_OF_ELEMENTS - 1; i >= 0; i--) {
            arrayList.remove(i);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("arrayList: " + res + " milliseconds");

        srartTime = System.currentTimeMillis();
        for (int i = NUMBER_OF_ELEMENTS - 1; i >= 0; i--) {
            linkedList.remove(i);
        }
        finishTime = System.currentTimeMillis();
        res = finishTime - srartTime;
        System.out.println("listLinked: " + res + " milliseconds\n");
    }
}